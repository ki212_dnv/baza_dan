BACKUP DATABASE GameStore
    TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\Backup\GameStore_FullDbBkup.bak' WITH INIT,
NAME = 'GameStore Full Db bakup',
DESCRIPTION = 'GameStore Full Database Backup'

RESTORE DATABASE GameStore
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\Backup\GameStore_FullDbBkup.bak'
WITH RECOVERY, REPLACE

BACKUP DATABASE GameStore
TO DISK ='C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\Backup\GameStore_FullDbBkup.bak'
WITH INIT, NAME = 'GameStore Full Db backup'

BACKUP LOG GameStore
to disk ='C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\Backup\GameStore_FullDbBkup.bak'
WITH NOINIT, NAME ='GameStore',
    DESCRIPTION = 'AdventureWorks Transaction Log Backup', NOFORMAT