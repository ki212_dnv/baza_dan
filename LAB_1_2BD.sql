use GameStore;
GO

CREATE PROCEDURE CountRows
AS
   DECLARE @tableName NVARCHAR(50) = '';
    DECLARE @numRows INT;
    DECLARE @sql NVARCHAR(MAX);

    DECLARE cursor_name CURSOR FOR
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES;

    BEGIN TRY
        CREATE TABLE DbRowCounts
        (
            TableName NVARCHAR(50),
            Count INT
        );
    END TRY
    BEGIN CATCH
        DROP TABLE DbRowCounts;
        CREATE TABLE DbRowCounts
        (
            TableName NVARCHAR(50),
            Count INT
        );
    END CATCH;

    OPEN cursor_name;
    FETCH NEXT FROM cursor_name INTO @tableName;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = CONCAT('SELECT @numRows = COUNT(*) FROM ', @tableName);

        EXEC SP_EXECUTESQL
             @Query = @sql,
             @Params = N'@numRows INT OUTPUT',
             @numRows = @numRows OUTPUT;

        IF @tableName NOT LIKE 'DbRowCounts'
            INSERT INTO DbRowCounts (TableName, Count) VALUES (@tableName, @numRows);

        FETCH NEXT FROM cursor_name INTO @tableName;
    END;

    CLOSE cursor_name;
    DEALLOCATE cursor_name;
END;
GO

EXEC CountRows;


CREATE PROCEDURE CountColumns
AS
BEGIN
    DECLARE @tableName NVARCHAR(50) = '';
    DECLARE @countColumns INT;

    DECLARE cursor_name CURSOR FOR
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES;

    BEGIN TRY
        CREATE TABLE DbColumnCounts
        (
            TableName NVARCHAR(50),
            Count INT
        );
    END TRY
    BEGIN CATCH
        DROP TABLE DbColumnCounts;
        CREATE TABLE DbColumnCounts
        (
            TableName NVARCHAR(50),
            Count INT
        );
    END CATCH;

    OPEN cursor_name;
    FETCH NEXT FROM cursor_name INTO @tableName;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SELECT @countColumns = COUNT(*)
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = @tableName;

        IF @tableName NOT LIKE 'DbColumnCounts'
            INSERT INTO DbColumnCounts (TableName, Count) VALUES (@tableName, @countColumns);

        FETCH NEXT FROM cursor_name INTO @tableName;
    END;

    CLOSE cursor_name;
    DEALLOCATE cursor_name;
END;
GO

EXEC CountColumns;


CREATE PROCEDURE UniqueDb
AS
BEGIN
    DECLARE @tableName NVARCHAR(50) = '';
    DECLARE @columnNameN NVARCHAR(50) = '';
    DECLARE @countColumns INT;
    DECLARE @sql NVARCHAR(MAX);

    DECLARE cursor_name CURSOR FOR
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES;

    BEGIN TRY
        CREATE TABLE DbUniqueValues
        (
            TableName NVARCHAR(50),
            ColumnName NVARCHAR(50),
            Count INT
        );
    END TRY
    BEGIN CATCH
        DROP TABLE DbUniqueValues;
        CREATE TABLE DbUniqueValues
        (
            TableName NVARCHAR(50),
            ColumnName NVARCHAR(50),
            Count INT
        );
    END CATCH;

    OPEN cursor_name;
    FETCH NEXT FROM cursor_name INTO @tableName;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE column_name CURSOR LOCAL FOR
            SELECT COLUMN_NAME
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = @tableName;

        OPEN column_name;
        FETCH NEXT FROM column_name INTO @columnNameN;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @sql = CONCAT('SELECT @countColumns = COUNT(DISTINCT ', @columnNameN, ') FROM ', @tableName);

            EXEC SP_EXECUTESQL
                 @Query = @sql,
                 @Params = N'@countColumns INT OUTPUT',
                 @countColumns = @countColumns OUTPUT;

            IF @tableName NOT LIKE 'DbUniqueValues'
                INSERT INTO DbUniqueValues (TableName, ColumnName, Count) VALUES (@tableName, @columnNameN, @countColumns);

            FETCH NEXT FROM column_name INTO @columnNameN;
        END;

        CLOSE column_name;
        DEALLOCATE column_name;

        FETCH NEXT FROM cursor_name INTO @tableName;
    END;

    CLOSE cursor_name;
    DEALLOCATE cursor_name;
END;
GO

EXEC UniqueDb;


CREATE TRIGGER PostForNewUser
    ON Customers
    AFTER INSERT
AS
BEGIN
    INSERT INTO Games (UserId, Title, ReleaseDate, Genre, Price, Description)
    SELECT Id_customer, 'Welcome Game', GETDATE(), 'Welcome Genre', 0.00, 'Welcome to the gaming community!'
    FROM inserted;
END;
GO

CREATE TRIGGER UserWithPosts
    ON Customers
    AFTER UPDATE
AS
BEGIN
    IF EXISTS (SELECT 1
               FROM inserted AS i
                        JOIN Games AS g ON i.Id_customer = g.UserId)
        BEGIN
            ROLLBACK TRANSACTION;
        END;
END;
GO


CREATE TRIGGER DelPostsWithAnswers
    ON Games
    AFTER DELETE
AS
BEGIN
    DECLARE @DeletedGameId INT;
    SELECT @DeletedGameId = Id_game FROM deleted;
    DELETE FROM Reviews WHERE GameId = @DeletedGameId;
    DELETE FROM Games WHERE Id_game = @DeletedGameId;
END;
GO


CREATE TRIGGER PreventDelUserWithPosts
    ON Customers
    INSTEAD OF DELETE
AS
BEGIN
    DECLARE @UserId INT;
    SELECT @UserId = Id_customer FROM deleted;

    IF EXISTS (SELECT 1
               FROM Games
               WHERE UserId = @UserId)
        BEGIN

        END
    ELSE
        BEGIN
            DELETE FROM Customers WHERE Id_customer = @UserId;
        END;
END;
GO
