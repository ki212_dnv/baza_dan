
CREATE DATABASE GameStore;
GO

USE GameStore;
GO

CREATE TABLE Publishers
(
    Id_publisher INT IDENTITY (1,1) PRIMARY KEY,
    Name         NVARCHAR(50),
    Address      NVARCHAR(50)
);
GO

CREATE TABLE Customers
(
    Id_customer  INT IDENTITY (1,1) PRIMARY KEY,
    Id_publisher INT NULL,
    FOREIGN KEY (Id_publisher) REFERENCES Publishers (Id_publisher),
    UserName     NVARCHAR(50),
    LastName     NVARCHAR(50),
    FirstName    NVARCHAR(50),
    Email        NVARCHAR(100),
    PhoneNumber  NVARCHAR(15),
    RatingScores INT
);
GO

CREATE TABLE Games
(
    Id_game      INT IDENTITY (1,1) PRIMARY KEY,
    UserId       INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Customers (Id_customer) ON DELETE CASCADE,
    Title        NVARCHAR(MAX) NOT NULL,
    ReleaseDate  DATETIME2 NOT NULL DEFAULT GETDATE(),
    Genre        NVARCHAR(50) NOT NULL,
    Price        DECIMAL(10,2) NOT NULL,
    Description  NVARCHAR(MAX) NOT NULL
);
GO

CREATE TABLE Reviews
(
    Id_review    INT IDENTITY (1,1) PRIMARY KEY,
    GameId       INT NOT NULL,
    FOREIGN KEY (GameId) REFERENCES Games (Id_game) ON DELETE CASCADE,
    UserId       INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES Customers (Id_customer) ON DELETE NO ACTION,
    Rating       INT NOT NULL CHECK (Rating BETWEEN 1 AND 5),
    DateCreate   DATETIME2 NOT NULL DEFAULT GETDATE(),
    Content      NVARCHAR(MAX) NOT NULL
);
GO

CREATE TABLE GameCategories
(
    Id_category INT IDENTITY (1,1) PRIMARY KEY,
    Name        NVARCHAR(50) NOT NULL
);
GO

CREATE TABLE GameCategoryMapping
(
    GameId     INT NOT NULL,
    FOREIGN KEY (GameId) REFERENCES Games (Id_game) ON DELETE CASCADE,
    CategoryId INT NOT NULL,
    FOREIGN KEY (CategoryId) REFERENCES GameCategories (Id_category) ON DELETE CASCADE,
    CONSTRAINT [PK_GameCategoryMapping] PRIMARY KEY CLUSTERED ([GameId], [CategoryId])
);
GO

INSERT INTO Publishers (Name, Address)
VALUES ('Ubisoft', '123 Game St.'),
       ('EA', '456 Gaming Ave.');

INSERT INTO Customers (Id_publisher, UserName, LastName, FirstName, Email, PhoneNumber, RatingScores)
VALUES (1, 'gamer1', 'Doe', 'John', 'john.doe@email.com', '(123)4567890', 5),
       (2, 'player2', 'Smith', 'Alice', 'alice.smith@email.com', '(123)4567432', 4);

INSERT INTO Games (UserId, Title, Genre, Price, Description)
VALUES (1, 'Assassin''s Creed Valhalla', 'Action-Adventure', 59.99, 'Explore the Viking age as Eivor, a fierce Viking warrior.');

INSERT INTO Reviews (GameId, UserId, Rating, Content)
VALUES (1, 2, 4, 'Great game! Enjoyed the storyline and gameplay.');

INSERT INTO GameCategories (Name)
VALUES ('Action-Adventure');

INSERT INTO GameCategoryMapping (GameId, CategoryId)
VALUES (1, 1);
