EXEC sp_Helpfile;
--OR
SELECT @@Servername AS Server, DB_NAME() AS DB_Name,
File_id, Type_desc, Name,
LEFT(Physical_Name, 1) AS Drive,
Physical_Name,
RIGHT(physical_name, 3) AS Ext,
Size,
Growth
FROM sys.database_files
ORDER BY File_id;

use GameStore;
SELECT *
FROM sys.objects
WHERE type = 'U';

select @@SERVERNAME AS ServerName ,
       DB_NAME() AS DBName ,
       t.Name AS TableName,
       t.[Type],
       t.create_date
FROM sys.tables t
ORDER BY t.NAME

select 'Select''' + DB_NAME()+'.'+SCHEMA_NAME(SCHEMA_ID)+'.'
+LEFT(o.name, 128) + ''' as DBName, count(*) as Count From' + SCHEMA_NAME(schema_id)+'.'+o.name
+ ';' AS 'Script generator to get conts for all tables'
FROM sys.objects o
WHERE o.[type]='U'
ORDER BY o.name;


SELECT @@ServerName AS Server ,
DB_NAME() AS DBName ,
OBJECT_SCHEMA_NAME(p.object_id) AS SchemaName ,
OBJECT_NAME(p.object_id) AS TableName ,
i.Type_Desc ,
i.Name AS IndexUsedForCounts ,
SUM(p.Rows) AS Rows
FROM sys.partitions p
JOIN sys.indexes i ON i.object_id = p.object_id
AND i.index_id = p.index_id
WHERE i.type_desc IN ( 'CLUSTERED', 'HEAP' )
-- This is key (1 index per table)
AND OBJECT_SCHEMA_NAME(p.object_id) <> 'sys'
GROUP BY p.object_id ,
i.type_desc ,
i.Name
ORDER BY SchemaName ,
TableName;
-- OR
-- ?????? ????? ????????? ????????? ???????, ??? ? ????????????? DMV dm_db_partition_stats
SELECT @@ServerName AS ServerName ,
DB_NAME() AS DBName ,
OBJECT_SCHEMA_NAME(ddps.object_id) AS SchemaName ,
OBJECT_NAME(ddps.object_id) AS TableName ,
i.Type_Desc ,
i.Name AS IndexUsedForCounts ,
SUM(ddps.row_count) AS Rows
FROM sys.dm_db_partition_stats ddps
JOIN sys.indexes i ON i.object_id = ddps.object_id
AND i.index_id = ddps.index_id
WHERE i.type_desc IN ( 'CLUSTERED', 'HEAP' )
-- This is key (1 index per table)
AND OBJECT_SCHEMA_NAME(ddps.object_id) <> 'sys'
GROUP BY ddps.object_id ,
i.type_desc ,
i.Name
ORDER BY SchemaName ,
TableName;
GO

select @@SERVERNAME AS ServerName ,
       DB_NAME() AS DBName ,
       o.Name AS StoredProcedureName,
       o.[Type],
       o.create_date
FROM sys.objects o
WHERE o.[Type] = 'P' --Stored Procedures
ORDER BY o.NAME

